package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) throws FileNotFoundException {

        new MainClass().run("", "");
    }

    private void run(String postsSourcePath, String authorsSourcePath) throws FileNotFoundException {

        Scanner scanner = new Scanner(new File(Paths.get(postsSourcePath).toUri()));
        int a = 0;
        while (scanner.hasNext()) {

            String line = scanner.nextLine();
            String[] split = line.split(",");

           ArrayList<Author> authors =  read(authorsSourcePath);
            long i = Integer.parseInt(split[1]);

            Post post = new Post(split[2], split[3], i, authors.get(a));
            allPosts.add(post);
            a++;
        }
        System.out.println(allPosts);
    }

    public ArrayList<Author> read(String authorsSourcePath) throws FileNotFoundException {
        ArrayList<Author> autArrayList = new ArrayList<>();

        Scanner scanner = new Scanner(new File(Paths.get(authorsSourcePath).toUri()));
        while (scanner.hasNext()) {

            String line = scanner.nextLine();
            String[] split = line.split(",");
            long i = Integer.parseInt(split[0]);

            Author author = new Author(i, split[1],split[2]);
            autArrayList.add(author);
        }
        return autArrayList;
    }
}
