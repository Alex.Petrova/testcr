package itis.socialtest;

import itis.socialtest.entities.Post;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class AnalyticsServiceImpl implements AnalyticsService {
    @Override
    public List<Post> findPostsByDate(List<Post> posts, String date) {
      return  posts.stream()
              .filter(post -> post.getDate().equals(date))
              .collect(Collectors.toList());
    }

    @Override
    public String findMostPopularAuthorNickname(List<Post> posts) {
        /*int i = posts.stream().mapToInt(posts -> posts.getLikesCount()).max().orElse(0);
        Optional<Post> first = posts.stream()
                .filter(post -> post.getLikesCount().equals(i)).findFirst();

         */
        return null;
    }

    @Override
    public Boolean checkPostsThatContainsSearchString(List<Post> posts, String searchString) {
        Post post1 = posts.stream().filter(post -> post.getContent().contains(searchString)).findAny().orElse(null);
        return post1 != null;
    }

    @Override
    public List<Post> findAllPostsByAuthorNickname(List<Post> posts, String nick){
        return posts.stream()
                .filter(post -> post.getAuthor().getNickname().equals(nick)).collect(Collectors.toList());

   }
}
